public class LCD{

    private String status;   
    private int volume;  
    private int brightness;   
    private String cable;

    public void setStatus(String s){
        status = s;
    }
        
    public void setVolume(int v){
        volume = v;
    }

    public void setBrightness(int b){
        brightness = b;
    }

    public void setCable(String c){
        cable = c;
    }

    public String turnOff(){
        return status;
    }

    public String turnOn(){
        return status;
    }

    public int volumeUp(){
        return volume;
    }

    public int volumeDown(int vol){
        return volume;
    }

    public int brightnessUp(int b){
        return brightness;
    }

    public int brightnessDown(int b){
        return brightness;
    }

    public String cableUp(){
        return cable;
    }

    public String cableDown(){
        return cable;
    }

    public void displayMessange(){
        System.out.println("------------------LCD------------------");
        System.out.println("Status LCD saat ini     : " + status);
        System.out.println("Volume yang digunakan   : " + volume);
        System.out.println("Brightness saat ini     : " + brightness);
        System.out.println("Cable yang digunakan    : " + cable);
    }
}